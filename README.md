Enigma machine encryption wrote in python with QT GUI. Program supports english alphabet for now - however it's not hardcoded and can be easily changed. To play the simulator, download all files in the project and run the file called "gui.py".
NOTE: ONLY CAPITAL LETTERS ARE ALLOWED.

![Screenshot_from_2022-05-20_23-00-03](/uploads/fee751308634ef97bddba545667a01c7/Screenshot_from_2022-05-20_23-00-03.png)
